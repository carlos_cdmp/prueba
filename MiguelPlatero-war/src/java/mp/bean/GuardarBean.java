/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mp.bean;


import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import mp.entity.Customer;
import mp.entity.DiscountCode;
import mp.entity.MicroMarket;
import mp.entity.PurchaseOrder;
import mp.facade.CustomerFacade;
import mp.facade.DiscountCodeFacade;
import mp.facade.MicroMarketFacade;

/**
 *
 * @author Portatil
 */
@ManagedBean
@SessionScoped
public class GuardarBean implements Serializable {

    @EJB
    private MicroMarketFacade microMarketFacade;

    @EJB
    private DiscountCodeFacade discountCodeFacade;
    
    @EJB
    private CustomerFacade customerFacade;
    
    protected List<Customer> listaCust;
    protected String idDescuento;
    protected Customer nuevo;
    protected List<DiscountCode> listaDescuentos;
    protected List<MicroMarket> listaMercados;
    protected String idZip;
    /**
     * Creates a new instance of GuardarBean
     */
    public GuardarBean() {
    
    }
    public void limpiarNuevo(){
        nuevo = new Customer();
    }
    @PostConstruct
    public void init(){
        idDescuento = null;
        idZip = null;
        limpiarNuevo();
        listaDescuentos = discountCodeFacade.findAll();
        listaMercados = microMarketFacade.findAll();
    }
    
    public void recuperarCustomer(){
        listaCust = customerFacade.findAll();
    }
    public String doGuardar(){
        nuevo.setCustomerId(customerFacade.getNextID()+1);
        nuevo.setDiscountCode(discountCodeFacade.find(idDescuento));
        nuevo.setZip(microMarketFacade.find(idZip));
        nuevo.setPurchaseOrderCollection(new ArrayList<PurchaseOrder>());
        customerFacade.create(nuevo);
        listaCust.add(nuevo);
        return ("editar");
    }
    public Customer getNuevo() {
        return nuevo;
    }

    public void setNuevo(Customer nuevo) {
        this.nuevo = nuevo;
    }

    public List<DiscountCode> getListaDescuentos() {
        return listaDescuentos;
    }

    public void setListaDescuentos(List<DiscountCode> listaDescuentos) {
        this.listaDescuentos = listaDescuentos;
    }

    public List<MicroMarket> getListaMercados() {
        return listaMercados;
    }

    public void setListaMercados(List<MicroMarket> listaMercados) {
        this.listaMercados = listaMercados;
    }

    public String getIdDescuento() {
        return idDescuento;
    }

    public void setIdDescuento(String idDescuento) {
        this.idDescuento = idDescuento;
    }

    public String getIdZip() {
        return idZip;
    }

    public void setIdZip(String idZip) {
        this.idZip = idZip;
    }

    public List<Customer> getListaCust() {
        return listaCust;
    }

    public void setListaCust(List<Customer> listaCust) {
        this.listaCust = listaCust;
    }

    public void doBorrar(Customer cus){
        Customer c = cus;
        listaCust.remove(c);
        customerFacade.remove(c);
        recuperarCustomer();
    }
    
    
    
    
    
}
